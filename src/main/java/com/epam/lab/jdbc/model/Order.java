package com.epam.lab.jdbc.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Order {
    private Integer id;
    private Double driverRate;
    private Double distance;
    private Timestamp datetime;
    private BigDecimal price;
    private Integer userId;
    private Integer driverId;
    private Integer departureLocationId;
    private Integer destinationLocationId;
    private Integer carId;

    public Order() {
    }

    public Order(Integer id, Double driverRate, Double distance, Timestamp datetime, BigDecimal price, Integer userId,
                 Integer driverId, Integer departureLocationId, Integer destinationLocationId, Integer carId) {
        this.id = id;
        this.driverRate = driverRate;
        this.distance = distance;
        this.datetime = datetime;
        this.price = price;
        this.userId = userId;
        this.driverId = driverId;
        this.departureLocationId = departureLocationId;
        this.destinationLocationId = destinationLocationId;
        this.carId = carId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getDriverRate() {
        return driverRate;
    }

    public void setDriverRate(Double driverRate) {
        this.driverRate = driverRate;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getDepartureLocationId() {
        return departureLocationId;
    }

    public void setDepartureLocationId(Integer departureLocationId) {
        this.departureLocationId = departureLocationId;
    }

    public Integer getDestinationLocationId() {
        return destinationLocationId;
    }

    public void setDestinationLocationId(Integer destinationLocationId) {
        this.destinationLocationId = destinationLocationId;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", driverRate=" + driverRate +
                ", distance=" + distance +
                ", datetime=" + datetime +
                ", price=" + price +
                ", userId=" + userId +
                ", driverId=" + driverId +
                ", carId=" + carId +
                ", departureLocationId=" + departureLocationId +
                ", destinationLocationId=" + destinationLocationId +
                '}';
    }
}
