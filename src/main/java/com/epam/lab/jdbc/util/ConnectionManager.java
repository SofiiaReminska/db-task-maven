package com.epam.lab.jdbc.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private static final String DATABASE_CONNECTION_URL = "jdbc:mysql://localhost:3306/uklondb?useSSL=false&useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC";
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final Logger LOGGER = LogManager.getLogger(ConnectionManager.class);

    private static Connection connection;

    private ConnectionManager() {
    }

    public static Connection getConnection() {
        return connection == null ? connection = initConnection() : connection;
    }

    public static void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error("Cannot close connection ", e);

            } finally {
                connection = null;
            }
        }
    }

    private static Connection initConnection() {
        Connection c = null;
        try {
            Class.forName(JDBC_DRIVER).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            LOGGER.error("Cannot get newInstance", e);
        }
        try {
            c = DriverManager.getConnection(DATABASE_CONNECTION_URL, "root", "root");
        } catch (SQLException e) {
            LOGGER.error("Cannot get connection", e);
        }
        return c;
    }
}
