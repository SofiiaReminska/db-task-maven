package com.epam.lab.jdbc.transformer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface GenericTransformer<T> {
    List<T> transformResultSetToList(ResultSet rs) throws SQLException;

    T transformResultSetToObject(ResultSet rs) throws SQLException;
}
