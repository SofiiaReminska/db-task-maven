package com.epam.lab.jdbc.service;

import com.epam.lab.jdbc.util.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MetaDataService {
    private static final Logger LOGGER = LogManager.getLogger(MetaDataService.class);
    private Connection connection = ConnectionManager.getConnection();
    private DatabaseMetaData metaData;

    public void printMetaData() throws SQLException {
        metaData = connection.getMetaData();
        printDBMetaData();
        printTableMetaData(getTablesMetaData());
    }

    private void printDBMetaData() throws SQLException {
        LOGGER.info("Database Product Name: {}", metaData.getDatabaseProductName());
        LOGGER.info("Database Product Version: {}", metaData.getDatabaseProductVersion());
        LOGGER.info("Logged User: {}", metaData.getUserName());
        LOGGER.info("JDBC Driver: {}", metaData.getDriverName());
        LOGGER.info("Driver Version: {}", metaData.getDriverVersion());
    }

    private ArrayList<String> getTablesMetaData() throws SQLException {
        String[] table = {"TABLE"};
        ResultSet rs = metaData.getTables("uklondb", "uklondb", "%", table);
        ArrayList<String> tableNames = new ArrayList<>();
        while (rs.next()) {
            tableNames.add(rs.getString("TABLE_NAME"));
        }
        rs.close();
        return tableNames;
    }

    private void printTableMetaData(ArrayList<String> tableNames) throws SQLException {
        ResultSet rs;
        for (String table : tableNames) {
            rs = metaData.getColumns(null, null, table, "%");
            LOGGER.info(table);
            while (rs.next()) {
                System.out.println(new StringBuilder()
                        .append(rs.getString("COLUMN_NAME"))
                        .append(" ")
                        .append(rs.getString("TYPE_NAME"))
                        .append(" ")
                        .append(rs.getString("COLUMN_SIZE"))
                        .toString());
            }
            System.out.println("\n");
            rs.close();
        }
    }
}
