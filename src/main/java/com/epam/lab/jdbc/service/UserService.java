package com.epam.lab.jdbc.service;

import com.epam.lab.jdbc.dao.UserDao;
import com.epam.lab.jdbc.model.User;
import com.epam.lab.jdbc.util.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class UserService {
    private static final Logger LOGGER = LogManager.getLogger(UserService.class);
    private UserDao userDao = new UserDao(ConnectionManager.getConnection());

    public void createSampleUser() throws SQLException {
        try {
            userDao.create(new User.UserBuilder()
                    .setName("David")
                    .setSurname("Backham")
                    .setPassword("backham")
                    .setPhone("555-5555555")
                    .setEmail("Backham@gmail.com")
                    .build());
        } catch (SQLException e) {
            LOGGER.error("Cannot create user ", e);
        }
        LOGGER.info(userDao.getAll());
    }
}
