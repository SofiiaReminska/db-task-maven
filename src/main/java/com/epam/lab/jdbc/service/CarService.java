package com.epam.lab.jdbc.service;

import com.epam.lab.jdbc.dao.CarDao;
import com.epam.lab.jdbc.model.Car;
import com.epam.lab.jdbc.util.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class CarService {
    private static final Logger LOGGER = LogManager.getLogger(CarService.class);
    private CarDao carDao = new CarDao(ConnectionManager.getConnection());

    public void createSampleCar() throws SQLException {
        try {
            carDao.create(new Car.CarBuilder()
                    .setModel("Kia")
                    .setNumber("ВС2345ВВ")
                    .setColor("blue")
                    .build());
        } catch (SQLException e) {
            LOGGER.error("Cannot create car ", e);
        }
        LOGGER.info(carDao.getAll());
    }
}
