package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.Driver;
import com.epam.lab.jdbc.transformer.DriverTransformer;

import java.sql.*;
import java.util.List;

public class DriverDao implements GenericDao<Driver, Integer> {
    private static final String SELECT_ALL_FROM_DRIVER_QUERY = "SELECT * FROM driver";
    private static final String SELECT_ALL_FROM_DRIVER_BY_ID = "SELECT * FROM driver WHERE id = ?";
    private static final String INSERT_INTO_DRIVER_QUERY = "INSERT INTO driver(name,surname,email,password,phone,rate) VALUES (?,?,?,?,?,?)";
    private static final String UPDATE_DRIVER_BY_ID_QUERY = "UPDATE driver SET name=?,surname=?,email=?,password=?,phone=?,rate=? WHERE id=?";
    private static final String DELETE_FROM_DRIVER_BY_ID_QUERY = "DELETE FROM driver WHERE id=?";
    private Connection connection;
    private DriverTransformer transformer;

    public DriverDao(Connection connection) {
        this.connection = connection;
        this.transformer = new DriverTransformer();
    }

    @Override
    public List<Driver> getAll() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(SELECT_ALL_FROM_DRIVER_QUERY);
        List<Driver> ret = transformer.transformResultSetToList(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public Driver getByPK(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL_FROM_DRIVER_BY_ID);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        Driver ret = transformer.transformResultSetToObject(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public int create(Driver obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                INSERT_INTO_DRIVER_QUERY);
        statement.setString(1, obj.getName());
        statement.setString(2, obj.getSurname());
        statement.setString(3, obj.getEmail());
        statement.setString(4, obj.getPassword());
        statement.setString(5, obj.getPhone());
        statement.setObject(6, obj.getRate());
        return statement.executeUpdate();
    }

    @Override
    public void update(Driver obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                UPDATE_DRIVER_BY_ID_QUERY);
        statement.setString(1, obj.getName());
        statement.setString(2, obj.getSurname());
        statement.setString(3, obj.getEmail());
        statement.setString(4, obj.getPassword());
        statement.setString(5,obj.getPhone());
        statement.setDouble(6, obj.getRate());
        statement.setInt(7, obj.getId());
        statement.executeUpdate();
    }

    @Override
    public void delete(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_FROM_DRIVER_BY_ID_QUERY);
        statement.setInt(1, id);
        statement.execute();
    }
}

