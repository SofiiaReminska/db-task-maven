package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.DriverCar;
import com.epam.lab.jdbc.transformer.DriverCarTransformer;

import java.sql.*;
import java.util.List;

public class DriverCarDao {
    private static final String SELECT_ALL_FROM_DRIVER_HAS_CAR_QUERY = "SELECT * FROM driver_has_car";
    private static final String INSERT_INTO_DRIVER_HAS_CAR_QUERY = "INSERT INTO driver_has_car(driver_id,car_id) VALUES (?,?)";
    private static final String UPDATE_DRIVER_HAS_CAR_QUERY = "UPDATE driver_has_car SET driver_id=?,car_id=? WHERE driver_id=? and car_id=? ";
    private static final String DELETE_FROM_DRIVER_BY_ID_QUERY = "DELETE FROM driver_has_car WHERE driver_id=? and car_id=?";
    private Connection connection;
    private DriverCarTransformer transformer;

    public DriverCarDao(Connection connection) {
        this.connection = connection;
        this.transformer = new DriverCarTransformer();
    }

    public List<DriverCar> getAll() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(SELECT_ALL_FROM_DRIVER_HAS_CAR_QUERY);
        List<DriverCar> ret = transformer.transformResultSetToList(rs);
        rs.close();
        statement.close();
        return ret;
    }

    public int create(DriverCar obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                INSERT_INTO_DRIVER_HAS_CAR_QUERY);
        statement.setInt(1, obj.getDriverId());
        statement.setInt(2, obj.getCarId());
        return statement.executeUpdate();
    }

    public void update(DriverCar oldObj, DriverCar newObj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                UPDATE_DRIVER_HAS_CAR_QUERY);
        statement.setInt(1, newObj.getDriverId());
        statement.setInt(2, newObj.getCarId());
        statement.setInt(3, oldObj.getDriverId());
        statement.setInt(4, oldObj.getCarId());
        statement.executeUpdate();
    }

    public void delete(DriverCar obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_FROM_DRIVER_BY_ID_QUERY);
        statement.setInt(1, obj.getDriverId());
        statement.setInt(2, obj.getCarId());
        statement.execute();
    }
}
